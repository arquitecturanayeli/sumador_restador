--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:12:36 09/04/2016
-- Design Name:   
-- Module Name:   C:/Users/jms-m/Desktop/ARQUI/Sumador_Restador/Simula.vhd
-- Project Name:  Sumador_Restador
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sumador_restador
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY Simula IS
END Simula;
 
ARCHITECTURE behavior OF Simula IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sumador_restador
    PORT(
         A : IN  std_logic_vector(3 downto 0);
         B : IN  std_logic_vector(3 downto 0);
         selector : IN  std_logic;
         S : OUT  std_logic_vector(3 downto 0);
         Cout : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal A : std_logic_vector(3 downto 0) := (others => '0');
   signal B : std_logic_vector(3 downto 0) := (others => '0');
   signal selector : std_logic := '0';

 	--Outputs
   signal S : std_logic_vector(3 downto 0);
   signal Cout : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sumador_restador PORT MAP (
          A => A,
          B => B,
          selector => selector,
          S => S,
          Cout => Cout
        );

   -- Clock process definitions
  
 

   -- Stimulus process
--  
--   stim_proc: process
--   begin		
--  wait for 100 ns;
--  A<="0101";
--  B<="0011";
--  selector<='0';
--   
--   end process;
	
--	   stim_proc1: process
--   begin		
--  wait for 100 ns;
--  A<="0001";
--  B<="0000";
--  selector<='1';
--    
--   end process;
--	
--	   stim_proc2: process
--   begin		
--  wait for 100 ns;
--  A<="0010";
--  B<="0100";
--  selector<='0';
--   
--   end process;
--	
--	   stim_proc3: process
--   begin		
--  wait for 100 ns;
--  A<="0110";
--  B<="0110";
--  selector<='1';
--   
--   end process;
--	
--	   stim_proc4: process
--   begin		
--  wait for 100 ns;
--  A<="0010";
--  B<="0101";
--  selector<='1';
--   
--   end process;
--
--   stim_proc5: process
--   begin		
--  wait for 100 ns;
--  A<="0001";
--  B<="0011";
--  selector<='0';
--   
--   end process;
--	
--	   stim_proc6: process
--   begin		
--  wait for 100 ns;
--  A<="0101";
--  B<="0010";
--  selector<='0';
--   
--   end process;
--	
--	   stim_proc7: process
--   begin		
--  wait for 100 ns;
--  A<="0011";
--  B<="0011";
--  selector<='0';
--   
--   end process;
--	
	   stim_proc8: process
   begin		
  wait for 100 ns;
  A<="0111";
  B<="0111";
  selector<='1';
   
   end process;
  
  
 

END;
