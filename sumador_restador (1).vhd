
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity sumador_restador is
	generic (
		n : integer := 4
	);
	Port(
		A, B: in std_logic_vector (n-1 downto 0);
		selector: in std_logic;
		S : out std_logic_vector (n-1 downto 0);
		Cout : out std_logic
	
	
	
	);
	
end sumador_restador;

architecture Behavioral of sumador_restador is


--begin
	
	--operacion: process(A,B,selector)
	signal Eb: std_logic_vector (n-1 downto 0);
	signal c: std_logic_vector (n downto 0);

	begin
		c(0) <= selector;	
		oper:	for j in 0 to n-1 generate
			Eb(j) <= B(j) xor selector;
			s(j)<=a(j) xor eb(j) xor c(j);
			c(j+1)<=(a(j) and eb(j))or (a(j) and c(j))or (eb(j) and c(j));
		end generate oper;
		Cout <= c(n);		
--	end process;

	


end Behavioral;

