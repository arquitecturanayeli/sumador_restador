
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name Sumador_Restador -dir "E:/Sumador_Restador/planAhead_run_5" -part xc7a100tcsg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "E:/Sumador_Restador/sumador_restador.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {E:/Sumador_Restador} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "sumador_restador.ucf" [current_fileset -constrset]
add_files [list {sumador_restador.ucf}] -fileset [get_property constrset [current_run]]
link_design
